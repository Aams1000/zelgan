import torch
import torch.nn as nn
from torchvision.models.inception import inception_v3
from typing import Tuple


class InceptionClassifier(nn.Module):

    def __init__(self, numClasses: int):
        super(InceptionClassifier, self).__init__()
        self.network: nn.Module = inception_v3(pretrained=True)
        for i, param in self.network.named_parameters():
            param.requires_grad = False

        # freeze all but last two components
        for i, param in self.network.Mixed_7c.named_parameters():
            param.requires_grad = True

        inputSize: int = self.network.fc.in_features
        self.network.fc = nn.Linear(inputSize, numClasses)
        self.loss: nn.Module = torch.nn.CrossEntropyLoss(reduction="mean")

    def forward(self, x: torch.Tensor, y: torch.Tensor) -> torch.Tensor:
        x = self.network(x)
        return self.loss(x[0], y) if isinstance(x, Tuple) else self.loss(x, y)
