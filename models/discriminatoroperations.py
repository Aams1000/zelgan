import torch
import torch.nn as nn
import torch.nn.functional as F
from models.generatorblock import GeneratorBlock
from models.torgbconv import ToRGBConv
from typing import List, Tuple, Dict


def rgbcPlusBlock(x: torch.Tensor, stage: int, blocks: nn.ModuleList, rgbcs: nn.ModuleList) -> torch.Tensor:
    x = rgbcs[stage](x)
    x = blocks[stage](x)
    return x


def computeWeightedOutput(x: torch.Tensor,
                          stage: int,
                          alpha: float,
                          scaleFactor: int,
                          blocks: nn.ModuleList,
                          rgbs: nn.ModuleList) -> torch.Tensor:
    # print(x[0, 0])
    # print(stage)
    # print(f"X: {x.shape}")
    scaleTuple: Tuple[int, int] = (scaleFactor, scaleFactor)
    previousOutput: torch.Tensor = rgbs[stage - 1](F.avg_pool2d(x, scaleTuple, stride=scaleTuple))
    # print(f"Previous: {previousOutput.shape}")
    # print(x[0, 0])
    newOutput: torch.Tensor = blocks[stage](rgbs[stage](x))  # if stage > 1 else rgbs[stage](x)
    # print(f"New: {newOutput.shape}")
    return torch.add((1 - alpha) * previousOutput, alpha * newOutput)

    # return torch.add((1 - alpha) * (F.interpolate(rgbs[stage - 1](x), scale_factor=scaleFactor, mode="nearest")), alpha * rgbs[stage](blocks[stage](x)))
