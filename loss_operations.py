import torch


# NOTE - this function is courtesy of Facebook Research
# https://github.com/facebookresearch/pytorch_GAN_zoo/blob/master/models/loss_criterions/gradient_losses.py
def WGANGPGradientPenalty(input, fake, discriminator, weight, stage: int, classIds: torch.Tensor=None, backward=True):
    r"""
    Gradient penalty as described in
    "Improved Training of Wasserstein GANs"
    https://arxiv.org/pdf/1704.00028.pdf
    Args:
        - input (Tensor): batch of real data
        - fake (Tensor): batch of generated data. Must have the same size
          as the input
        - discrimator (nn.Module): discriminator network
        - weight (float): weight to apply to the penalty term
        - backward (bool): loss backpropagation
    """

    batchSize = input.size(0)
    alpha = torch.rand(batchSize, 1)
    alpha = alpha.expand(batchSize, int(input.nelement() /
                                        batchSize)).contiguous().view(
        input.size())
    alpha = alpha.to(input.device)
    interpolates = alpha * input + ((1 - alpha) * fake)

    interpolates = torch.autograd.Variable(
        interpolates, requires_grad=True)
    # print(interpolates.shape)
    decisionInterpolate = discriminator(interpolates, stage)[0] if classIds is None else discriminator(interpolates, classIds, stage)
    # print(decisionInterpolate.shape)
    decisionInterpolate = decisionInterpolate[:, 0].sum()
    # print(decisionInterpolate)

    gradients = torch.autograd.grad(outputs=decisionInterpolate,
                                    inputs=interpolates,
                                    # grad_outputs=torch.ones(decisionInterpolate.size(), device=device),
                                    create_graph=True, retain_graph=True)
    # print(gradients[0])
    # print(gradients[1])
    gradients = gradients[0].view(batchSize, -1)
    # print(gradients.shape)
    gradients = (gradients * gradients).sum(dim=1).sqrt()
    gradient_penalty = (((gradients - 1.0)**2)).mean() * weight

    if backward:
        gradient_penalty.backward(retain_graph=True)

    return gradient_penalty.item()

