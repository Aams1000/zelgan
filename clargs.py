import torch


class CLArgs(object):

    class Device:
        CUDA = "cuda"
        CPU = "cpu"

    def __init__(self, args):
        self.argsMap = args
        self.CUDA_FLAG = "--cuda"
        self.SAMPLE_FLAG: str = "--sample"
        # self.TEST_FLAG = "--test"
        self.FLAG_RESUME = "--resume"
        self.FLAG_NEXT_STAGE: str = "--nextStage"
        self.OVERRIDE_ALPHA: str = "--overrideAlpha"
        self.TRAIN_FID: str = "--trainFID"
        self.FORCE_NEW_OPTIMIZERS: str = "--forceNewOptimizers"
        self.COMPUTE_FID: str = "--computeFID"
        # self.FORCE_LR_FLAG = "--forceLr"
        # self.BASELINE_FLAG = "--baseline"

    def getDevice(self) -> torch.device:
        deviceString = None
        if self.argsMap[self.CUDA_FLAG]:
            deviceString = self.Device.CUDA
        else:
            deviceString = self.Device.CPU
        return torch.device(deviceString)

    # def getRunmode(self) -> str:
    #     if self.argsMap[self.Runmode.TRAIN]:
    #         return self.Runmode.TRAIN
    #     elif self.argsMap[self.Runmode.TEST]:
    #         return self.Runmode.TEST
    #     elif self.argsMap[self.Runmode.TOY]:
    #         return self.Runmode.TOY
    #     elif self.argsMap[self.Runmode.TINY]:
    #         return self.Runmode.TINY
    #     elif self.argsMap[self.Runmode.TEST_ONE_IMAGE]:
    #         return self.Runmode.TEST_ONE_IMAGE
    #     elif self.argsMap[self.Runmode.CALCULATE_MEANS]:
    #         return self.Runmode.CALCULATE_MEANS
    #     elif self.argsMap[self.Runmode.VALIDATE]:
    #         return self.Runmode.VALIDATE
    #     else:
    #         raise RuntimeError("Invalid runmode. Please include --train, --test, or --toy")


    def isResumeRun(self) -> bool:
        return self.argsMap[self.FLAG_RESUME]

    def isSampleRun(self) -> bool:
        return self.argsMap[self.SAMPLE_FLAG]

    def startNextStage(self) -> bool:
        return self.argsMap[self.FLAG_NEXT_STAGE]

    def overrideAlpha(self) -> bool:
        return self.argsMap[self.OVERRIDE_ALPHA]

    def trainFID(self) -> bool:
        return self.argsMap[self.TRAIN_FID]

    def forceNewOptimizers(self) -> bool:
        return self.argsMap[self.FORCE_NEW_OPTIMIZERS]

    def computeFID(self) -> bool:
        return self.argsMap[self.COMPUTE_FID]
    # def shouldForceNewLearningRate(self) -> bool:
    #     return self.argsMap[self.FORCE_LR_FLAG]

