import torch
import torch.nn as nn
import torch.nn.functional as F
import models.weightscaling as weightscaling
import math
from fileio.customlogging import Logging


class FromRGBCConv(nn.Module):

    def __init__(self,
                 outChannels: int,
                 inChannels: int = 4,
                 leakyReLUAlpha: float = 0.2):
        super(FromRGBCConv, self).__init__()
        self.conv: nn.Conv2d = nn.Conv2d(inChannels, outChannels, kernel_size=1)
        nn.init.normal_(self.conv.weight)
        torch.nn.init.zeros_(self.conv.bias)
        self.weightScale: float = weightscaling.computeWeightScale(self.conv)
        self.leakyReLUAlpha: float = leakyReLUAlpha

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        # Logging.log(self.conv.bias)
        if math.isnan(x[0, 0, 0, 0].item()):
            Logging.log("NAN")
        x = self.conv(x) * self.weightScale
        # if math.isnan(x[0, 0, 0, 0].item()):
        # Logging.log(self.weightScale)
        # Logging.log(self.conv.weight)
        # Logging.log(self.conv.bias)
        # Logging.log("NAN")
        x = F.leaky_relu(x, self.leakyReLUAlpha)
        if math.isnan(x[0, 0, 0, 0].item()):
            Logging.log("NAN")
        return x

if __name__ == "__main__":
    torch.manual_seed(5)
    testInput: torch.Tensor = torch.rand((1, 4, 2, 2))
    assert(FromRGBCConv(311)(testInput).shape == (1, 311, 2, 2))