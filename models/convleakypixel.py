import torch
import torch.nn as nn
import torch.nn.functional as F
import models.weightscaling as weightscaling
import math
from fileio.customlogging import Logging


class ConvLeakyPixel(nn.Module):

    def __init__(self,
                 inChannels: int,
                 outChannels: int,
                 kernelSize: int = 3,
                 padding: int = 1,
                 stride: int = 1,
                 usePixelNorm: bool = True,
                 leakyReLUAlpha: float = 0.2):
        super(ConvLeakyPixel, self).__init__()
        self.conv: nn.Conv2d = nn.Conv2d(inChannels, outChannels, kernelSize, stride, padding)
        nn.init.normal_(self.conv.weight)
        torch.nn.init.zeros_(self.conv.bias)
        self.weightScale: float = weightscaling.computeWeightScale(self.conv)
        self.usePixelNorm: bool = usePixelNorm
        self.leakyReLUAlpha: float = leakyReLUAlpha

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        #TODO - use no_grad? is this right?
        if math.isnan(x[0, 0, 0, 0].item()):
            Logging.log("NAN")
        x = self.conv(x) * self.weightScale
        x = F.leaky_relu_(x, self.leakyReLUAlpha)
        if self.usePixelNorm:
            x = weightscaling.pixelNorm(x)
        return x

