from collections import deque
from typing import Deque


class LossContainer(object):
    def __init__(self):
        pass

    def generateLogString(self) -> str:
        pass

    def mean(self, queue: Deque[float]) -> float:
        avg: float = 0.0
        for f in queue:
            avg += f
        return avg / len(queue)


class WassersteinACLoss(LossContainer):

    def __init__(self, numItems: int, roundLength: int = 3):
        super(WassersteinACLoss, self).__init__()
        self.roundLength: int = roundLength
        self.genLossPred: Deque[float] = deque(maxlen=numItems)
        self.genLossClass: Deque[float] = deque(maxlen=numItems)
        self.discLossPred: Deque[float] = deque(maxlen=numItems)
        self.discLossRealClass: Deque[float] = deque(maxlen=numItems)
        self.discLossFakeClass: Deque[float] = deque(maxlen=numItems)
        self.discLossGrad: Deque[float] = deque(maxlen=numItems)
        self.discLossEps: Deque[float] = deque(maxlen=numItems)

    def generateLogString(self) -> str:
        # lossNames: List[str] = ["GP", "GC", "DRP", "DRC", "DFP", "DFC"]
        return f"GP: {round(self.mean(self.genLossPred), self.roundLength)}, " \
               f"GC: {round(self.mean(self.genLossClass), self.roundLength)}, " \
               f"DP: {round(self.mean(self.discLossPred), self.roundLength)}, " \
               f"DG: {round(self.mean(self.discLossGrad), self.roundLength)}, " \
               f"DRC: {round(self.mean(self.discLossRealClass), self.roundLength)}, " \
               f"DFC: {round(self.mean(self.discLossFakeClass), self.roundLength)}, " \
               f"DE: {round(self.mean(self.discLossEps), self.roundLength)}"


class WassersteinNonACLoss(LossContainer):

    def __init__(self, numItems: int, roundLength: int = 3):
        super(WassersteinNonACLoss, self).__init__()
        self.roundLength: int = roundLength
        self.genLossPred: Deque[float] = deque(maxlen=numItems)
        self.discLossPred: Deque[float] = deque(maxlen=numItems)
        self.discLossGrad: Deque[float] = deque(maxlen=numItems)
        self.discLossEps: Deque[float] = deque(maxlen=numItems)

    def generateLogString(self) -> str:
        # lossNames: List[str] = ["GP", "GC", "DRP", "DRC", "DFP", "DFC"]
        return f"GP: {round(self.mean(self.genLossPred), self.roundLength)}, " \
               f"DP: {round(self.mean(self.discLossPred), self.roundLength)}, " \
               f"DG: {round(self.mean(self.discLossGrad), self.roundLength)}, " \
               f"DE: {round(self.mean(self.discLossEps), self.roundLength)}"


class ACLoss(LossContainer):

    def __init__(self,  numItems: int, roundLength: int = 3):
        super(ACLoss, self).__init__()
        self.roundLength: int = roundLength
        self.genLossPred: Deque[float] = deque(maxlen=numItems)
        self.genLossClass: Deque[float] = deque(maxlen=numItems)
        self.discLossRealPred: Deque[float] = deque(maxlen=numItems)
        self.discLossRealClass: Deque[float] = deque(maxlen=numItems)
        self.discLossFakePred: Deque[float] = deque(maxlen=numItems)
        self.discLossFakeClass: Deque[float] = deque(maxlen=numItems)

    def generateLogString(self) -> str:
        # lossNames: List[str] = ["GP", "GC", "DRP", "DRC", "DFP", "DFC"]
        return f"GP: {round(self.mean(self.genLossPred), self.roundLength)}, " \
               f"GC: {round(self.mean(self.genLossClass), self.roundLength)}, " \
               f"DRP: {round(self.mean(self.discLossRealPred), self.roundLength)}, " \
               f"DRC: {round(self.mean(self.discLossRealClass), self.roundLength)}, " \
               f"DFP: {round(self.mean(self.discLossFakePred), self.roundLength)}, " \
               f"DFC: {round(self.mean(self.discLossFakeClass), self.roundLength)}"


class InceptionClassifierLoss(LossContainer):

    def __init__(self,  numItems: int, roundLength: int = 3):
        super(InceptionClassifierLoss, self).__init__()
        self.roundLength: int = roundLength
        self.loss: Deque[float] = deque(maxlen=numItems)

    def generateLogString(self) -> str:
        # lossNames: List[str] = ["GP", "GC", "DRP", "DRC", "DFP", "DFC"]
        return f"Loss: {round(self.mean(self.loss), self.roundLength)}"

