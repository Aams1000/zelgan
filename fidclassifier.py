#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from typing import *

from fileio.customlogging import Logging
import torch
from torch.utils.data import DataLoader

from clargs import CLArgs
from config.config import Config
from fileio.loaders.dataset import Dataset
import math
import datalabels.class_ids as class_id_util
import models.model_loader as model_loader
import fileio.loaders.image_io as image_io
import fileio.loaders.model_io as model_io
from loss_containers import LossContainer, InceptionClassifierLoss
import loss_operations as loss_ops
from models.inception_classifier import InceptionClassifier
from torchvision.models.inception import inception_v3

import torch.nn as nn
from models.identity import Identity

import fid

def trainFidClassifier(runId: str,
                       stage: int,
                       device: torch.device) -> None:
    Logging.log("Initializing FID classifier for training")

    model: InceptionClassifier = InceptionClassifier(class_id_util.getNumClassIds())
    model = model.to(device)
    optimizer: torch.optim.Optimizer = torch.optim.Adam(model.parameters(), Config.NN.LEARNING_RATE_FID, weight_decay=Config.NN.REGULARIZATION)
    datasetParameters: Dict[str, object] = {'batch_size': Config.NN.BATCH_SIZE,
                                            'shuffle': True,
                                            'num_workers': Config.IO.NUM_DATALOADER_THREADS}

    dataLoader: DataLoader = DataLoader(Dataset(stage, Config.NN.FID_DIMENSIONS), **datasetParameters)

    bestValidationLoss: float = float("inf")
    epoch: int = 1
    while epoch < Config.NN.EPOCHS_PER_STAGE + 1:
        # fail safe
        model.train()
        lossContainer: InceptionClassifierLoss = InceptionClassifierLoss(Config.NN.NUM_ITERATIONS_FOR_AVERAGE)
        iterations = 0
        Logging.log(f"Commencing stage {stage} epoch {epoch}")
        for realInputs, realClassIds in dataLoader:
            iterations += 1
            realInputs = realInputs.to(device)
            realClassIds = realClassIds.to(device)

            optimizer.zero_grad()
            loss: torch.Tensor = model(realInputs, realClassIds)
            loss.backward()
            optimizer.step()
            lossContainer.loss.append(loss.item())

            if iterations % Config.NN.LOG_FREQUENCY == 0:
                logAverageLosses(stage, epoch, iterations, lossContainer)

        Logging.log(f"Finished epoch {epoch}")
        logAverageLosses(stage, epoch, iterations, lossContainer)

        if epoch % Config.NN.VALIDATE_FREQUENCY == 0:
            Logging.log(f"Validating model from epoch {epoch}")
            valLoss: float = validate(model, stage, datasetParameters, device)
            if valLoss < bestValidationLoss:
                Logging.log(f"New best val loss: {valLoss}. Saving model.")
                bestValidationLoss = valLoss
                model_io.saveInceptionClassifierAndOptimizer(model.state_dict(), optimizer.state_dict(), stage, runId)
        epoch += 1

    Logging.log(f"Validating model from final epoch {epoch}")
    valLoss: float = validate(model, stage, datasetParameters, device)
    if valLoss < bestValidationLoss:
        Logging.log(f"New best val loss: {valLoss}. Saving model.")
        model_io.saveInceptionClassifierAndOptimizer(model.state_dict(), optimizer.state_dict(), stage, runId)
    Logging.log("Finished training. Check logs at " + str(Logging.LOG_FILE))


def validate(model: InceptionClassifier, stage: int, datasetParameters: Dict, device: torch.device) -> float:
    with torch.no_grad():
        model.eval()
        valLoader: DataLoader = DataLoader(Dataset(stage, Config.NN.FID_DIMENSIONS, isFIDValidation=True), **datasetParameters)
        valIterations: int = 0
        valLoss: float = 0.0
        for valInputs, valClassIds in valLoader:
            valIterations += 1
            valInputs = valInputs.to(device)
            valClassIds = valClassIds.to(device)
            valLoss += model(valInputs, valClassIds).item()
        valLoss /= valIterations
    return valLoss


def logAverageLosses(stage: int, epoch: int, iterations: int, lossContainer: LossContainer) -> None:
    Logging.log(f"Stage {stage}, epoch {epoch}, iter {iterations}. {lossContainer.generateLogString()}")


def computeFID(modelSavePath: str, pathOne: str, pathTwo: str, batchSize: int, device: torch.device, outputSize: int) -> None:
    # looks like I scp'd the optimizer state dict from my GCloud VM instead of the model state dict,
    # and now I'm out of credits. Guess I'll use a pretrained InceptionV3 instead of the one I trained
    # on the original data set
    # classifierModel: InceptionClassifier = InceptionClassifier(class_id_util.getNumClassIds())
    # classifierModel.load_state_dict(model_io.loadInceptionClassifier(modelSavePath, device))
    # inceptionModel: nn.Module = classifierModel.network
    inceptionModel: nn.Module = inception_v3(pretrained=True)
    inceptionModel.fc = Identity()
    inceptionModel.eval()
    fidScore: float = fid.calculate_fid_given_paths((pathOne, pathTwo), inceptionModel, batchSize, str(device) != "cpu", outputSize)
    Logging.log(f"FID score: {fidScore}")















