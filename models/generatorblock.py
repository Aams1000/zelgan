import torch
import torch.nn as nn
import torch.nn.functional as F
from models.convleakypixel import ConvLeakyPixel
from typing import Tuple


class GeneratorBlock(nn.Module):

    def __init__(self,
                 inChannels: int,
                 outChannels: int,
                 kernelSize: int = 3,
                 padding: int = 1,
                 stride: int = 1):
        super(GeneratorBlock, self).__init__()
        # print(inChannels, outChannels)
        self.clpOne: ConvLeakyPixel = ConvLeakyPixel(inChannels, outChannels, kernelSize, padding, stride)
        self.clpTwo: ConvLeakyPixel = ConvLeakyPixel(outChannels, outChannels, kernelSize, padding, stride)
        self.upscaleFactor: Tuple[float, float] = (2.0, 2.0)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        # print(f"Input: {x.shape}")
        x = F.interpolate(x, scale_factor=self.upscaleFactor, mode="nearest")
        x = self.clpOne(x)
        x = self.clpTwo(x)
        # print(f"Output: {x.shape}")
        return x


if __name__ == "__main__":
    testInput: torch.Tensor = torch.rand((2, 4, 5, 5))
    testBlock: GeneratorBlock = GeneratorBlock(4, 2)
    assert(testBlock(testInput).shape == (2, 2, 10, 10))


