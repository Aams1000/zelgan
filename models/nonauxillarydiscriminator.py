import torch
import torch.nn as nn
from models.final_discriminator_block import FinalDiscriminatorBlock
from models.fromrgbcconv import FromRGBCConv
from models.discriminatorblock import DiscriminatorBlock
import models.discriminatoroperations as disc_ops
import models.generator_operations as gen_ops
from typing import Tuple, Dict
from models.rescalingembedding import RescalingEmbedding
import models.weightscaling as weightscaling


class NonAuxillaryDiscriminator(nn.Module):

    __CLASS_ID_RESCALING_FACTOR: Tuple[float, float] = (0.0, 255.0)

    def __init__(self,
                 numClassIds: int,
                 classIdEmbeddingSize: int = 16,
                 maxChannels: int = 512,
                 numMainBlocks: int = 6,
                 alphaIncrement: float = 0.000125,
                 scaleFactor: int = 2,
                 initialStage: int = 0):
        super(NonAuxillaryDiscriminator, self).__init__()
        self.classIdEmbedding: RescalingEmbedding = RescalingEmbedding(numClassIds, classIdEmbeddingSize, NonAuxillaryDiscriminator.__CLASS_ID_RESCALING_FACTOR)
        self.discriminatorBlocks: nn.ModuleList = nn.ModuleList([FinalDiscriminatorBlock(maxChannels)])
        self.rgbcBlocks: nn.ModuleList = nn.ModuleList([FromRGBCConv(maxChannels)])

        blocksPlusRgbcs: Tuple[nn.ModuleList, nn.ModuleList] = self.constructMainBlocksAndRGBs(maxChannels, numMainBlocks)
        self.discriminatorBlocks.extend(blocksPlusRgbcs[0])
        self.rgbcBlocks.extend(blocksPlusRgbcs[1])
        self.scaleFactor: int = scaleFactor
        self.alphaIncrement: float = alphaIncrement

        # fade-in applies only to blockIds >= 1. Using a dictionary to avoid confusing indexing
        self.alphas: Dict[int, float] = gen_ops.initializeAlphas(initialStage, alphaIncrement, numMainBlocks)

    @staticmethod
    def constructMainBlocksAndRGBs(maxChannels: int, numMainBlocks: int) -> Tuple[nn.ModuleList, nn.ModuleList]:
        discriminatorBlocks: nn.ModuleList = nn.ModuleList()
        fromRgbcBlocks: nn.ModuleList = nn.ModuleList()
        for i in range(0, numMainBlocks):
            inputChannels: int = maxChannels // 2**(i)
            outputChannels: int = min(inputChannels * 2, maxChannels)
            fromRgbcBlocks.append(FromRGBCConv(inputChannels))
            discriminatorBlocks.append(DiscriminatorBlock(inputChannels, outputChannels))

        return discriminatorBlocks, fromRgbcBlocks

    def forward(self, x: torch.Tensor, classIds: torch.Tensor, stage: int) -> torch.Tensor:
        N, C, H, W = x.shape
        classIds = self.classIdEmbedding(classIds)
        classIds = weightscaling.reshapeAndExpandEmbedding(classIds, H)
        x = torch.cat((x, classIds), dim=1)
        # if stage == 0:
        #     x = disc_ops.rgbcPlusBlock(x, stage, self.discriminatorBlocks, self.rgbcBlocks)
        # else:
        #     alpha: float = self.alphas[stage]
        #     if alpha < 1.0:
        #         # print("Scaling...")
        #         x = disc_ops.computeWeightedOutput(x, stage, alpha, self.scaleFactor, self.discriminatorBlocks, self.rgbcBlocks)
        #         gen_ops.updateAlpha(self.alphas, stage, x.shape[0], self.alphaIncrement)
        #     else:
        #         # print(f"X: {x.shape}")
        #         x = disc_ops.rgbcPlusBlock(x, stage, self.discriminatorBlocks, self.rgbcBlocks)
        #     for i in reversed(range(0, stage)):
        #         # print(f"Executing block {i}...")
        #         x = self.discriminatorBlocks[i](x)
        #
        # return x
        if stage > 0:
            alpha: float = self.alphas[stage]
            if alpha < 1.0:
                # print("Scaling d...")
                x = disc_ops.computeWeightedOutput(x, stage, alpha, self.scaleFactor, self.discriminatorBlocks, self.rgbcBlocks)
                gen_ops.updateAlpha(self.alphas, stage, x.shape[0], self.alphaIncrement)
            else:
                # print(f"X: {x.shape}")
                x = disc_ops.rgbcPlusBlock(x, stage, self.discriminatorBlocks, self.rgbcBlocks)
            for i in reversed(range(1, stage)):
                # print(f"Executing block {i}...")
                x = self.discriminatorBlocks[i](x)
        else:
            x = self.rgbcBlocks[stage](x)

        return self.discriminatorBlocks[0](x)[0]


if __name__ == "__main__":
    testInput: torch.Tensor = torch.rand((4, 3, 32, 32))
    (NonAuxillaryDiscriminator(10))(testInput, torch.ones((4, 1)).long(), 3)


