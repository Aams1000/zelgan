#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Usage:
    run.py [options]


Options:
    -h --help                               show this screen.
    --cuda                                  use GPU
    --train                                 Train on the full training set
    --test                                  Test on the full test set
    --toy                                   Train on the toy dataset
    --tiny                                  Train on one-example dataset
    --resume                                Resume from pre-trained model
    --validate                              Run validation
    --sample                                Generate samples from pre-trained model
    --nextStage                             Start next stage for loaded model
    --overrideAlpha                         Set alpha to one for starting stage
    --trainFID                              Train FID classifier
    --forceNewOptimizers                    Force new optimizers
    --computeFID                            Compute FID score
"""

from docopt import docopt

import time
import datetime
from typing import *

from fileio.customlogging import Logging
import torch
from torch.utils.data import DataLoader

from clargs import CLArgs
from config.config import Config
from fileio.loaders.dataset import Dataset
import datalabels.class_ids as class_id_util
import models.model_loader as model_loader
import fileio.loaders.image_io as image_io
import fileio.loaders.model_io as model_io
from loss_containers import WassersteinACLoss, ACLoss, WassersteinNonACLoss, LossContainer
import loss_operations as loss_ops
import fidclassifier as fid_classifier


def sample(runId: str,
           savedModelPath: str,
           sampleSize: int,
           stage: int,
           device: torch.device,
           useAuxillaryDisc: bool = True) -> None:
    numClassIds: int = class_id_util.getNumClassIds()

    generator: torch.nn.Module = None
    discriminator: torch.nn.Module = None
    generator, discriminator = model_loader.loadGenAndDisc(stage,
                                                           Config.NN.LATENT_CHANNELS,
                                                           Config.NN.LATENT_FEATURES,
                                                           Config.NN.INITIAL_RESOLUTION,
                                                           numClassIds,
                                                           Config.NN.ALPHA_INCREMENT,
                                                           device,
                                                           savedModelPath=savedModelPath,
                                                           shouldUseAuxillaryDisc=useAuxillaryDisc,
                                                           overrideAlpha=True)
    generator.eval()
    discriminator.eval()

    generator.module.alphas[stage] = 0.5

    Logging.log(f"Saving {sampleSize} samples per class")

    for i in reversed(range(0, numClassIds)):
        testLatents: torch.Tensor = torch.randn(sampleSize, Config.NN.LATENT_CHANNELS, 1, device=device)
        testClassIds: torch.Tensor = torch.zeros((sampleSize, 1), device=device).long() + i

        with torch.no_grad():
            image_io.savePredictions(generator(testLatents, testClassIds, stage), testClassIds, runId, stage, -1, randomInputs=True)


def train(runId: str,
          device: torch.device,
          stage: int = 0,
          savedModelPath: str = None,
          loadNewOptimizers: bool = False,
          overrideAlpha: bool = False,
          useAuxillaryDisc: bool = True,
          useWassersteinLoss: bool = True) -> None:
    Logging.log("Initializing models for training")
    Logging.log(f"Auxilary run: {useAuxillaryDisc}")
    Logging.log(f"Wasserstein loss: {useWassersteinLoss}")
    Logging.log(f"Learning rate: {Config.NN.LEARNING_RATE}")
    Logging.log(f"Betas: {Config.NN.BETAS}")

    numClassIds: int = class_id_util.getNumClassIds()

    generator: torch.nn.Module = None
    discriminator: torch.nn.Module = None
    generator, discriminator = model_loader.loadGenAndDisc(stage,
                                                           Config.NN.LATENT_CHANNELS,
                                                           Config.NN.LATENT_FEATURES,
                                                           Config.NN.INITIAL_RESOLUTION,
                                                           numClassIds,
                                                           Config.NN.ALPHA_INCREMENT,
                                                           device,
                                                           savedModelPath=savedModelPath,
                                                           shouldUseAuxillaryDisc=useAuxillaryDisc,
                                                           overrideAlpha=overrideAlpha)

    reduction: str = "mean"
    veracityLoss: torch.nn.BCEWithLogitsLoss = torch.nn.BCEWithLogitsLoss(reduction=reduction)
    classificationLoss: torch.nn.CrossEntropyLoss = torch.nn.CrossEntropyLoss(reduction=reduction)

    testLatents: torch.Tensor = torch.randn(Config.NN.TEST_OUTPUT_BATCH_SIZE, Config.NN.LATENT_CHANNELS, 1, device=device)
    testClassIds: torch.Tensor = torch.arange(Config.NN.TEST_OUTPUT_BATCH_SIZE, device=device).unsqueeze(1) % numClassIds

    genOptimizer: torch.optim.Optimizer = None
    discOptimizer: torch.optim.Optimizer = None
    while stage < Config.NN.MAX_STAGE:
        Logging.log(f"Commencing stage {stage}")
        timesSaved: float = 0.0
        genModelPath: str = savedModelPath if genOptimizer is None and discOptimizer is None and not loadNewOptimizers else None
        genOptimizer, discOptimizer = model_loader.loadGenAndDiscOptimizers(generator, discriminator, Config.NN.LEARNING_RATE, Config.NN.BETAS, device, genModelPath)

        datasetParameters: Dict[str, object] = {'batch_size': Config.NN.BATCH_SIZE,
                                                 'shuffle': True,
                                                 'num_workers': Config.IO.NUM_DATALOADER_THREADS}

        dataLoader: DataLoader = DataLoader(Dataset(stage), **datasetParameters)

        # fail safe
        generator.train()
        discriminator.train()

        # Loop over epochs
        if useWassersteinLoss:
            if useAuxillaryDisc:
                lossContainer: WassersteinACLoss = WassersteinACLoss(Config.NN.NUM_ITERATIONS_FOR_AVERAGE)
            else:
                lossContainer: WassersteinNonACLoss = WassersteinNonACLoss(Config.NN.NUM_ITERATIONS_FOR_AVERAGE)
        else:
            lossContainer: ACLoss = ACLoss(Config.NN.NUM_ITERATIONS_FOR_AVERAGE)
        epoch: int = 1
        while epoch < Config.NN.EPOCHS_PER_STAGE + 1:
            iterations = 0
            Logging.log(f"Commencing stage {stage} epoch {epoch}")
            for realInputs, realClassIds in dataLoader:
                iterations += 1
                realInputs = realInputs.to(device)
                if useAuxillaryDisc:
                    realClassIds = realClassIds.to(device)
                else:
                    realClassIds = realClassIds.unsqueeze(1).to(device)

                batchSize: int = realInputs.shape[0]
                # lame restriction, will fix if have time. Last iteration of epoch will be skipped
                if batchSize % Config.NN.INITIAL_RESOLUTION == 0:

                    # handle real data
                    discOptimizer.zero_grad()
                    realPredictions: torch.Tensor = None
                    realClassifications: torch.Tensor = None
                    if useAuxillaryDisc:
                        realPredictions, realClassifications = discriminator(realInputs, stage)
                    else:
                        realPredictions = discriminator(realInputs, realClassIds, stage)
                    discLossRealClass: torch.Tensor = None
                    if realClassifications is not None:
                        discLossRealClass: torch.Tensor = classificationLoss(realClassifications, realClassIds)
                    if not useWassersteinLoss:
                        discLossRealPred: torch.Tensor = veracityLoss(realPredictions, class_id_util.generateRealLabels(batchSize, device)) / 4
                        lossContainer.discLossRealPred.append(discLossRealPred.item())
                        loss: torch.Tensor = discLossRealPred
                        if discLossRealClass is not None:
                            discLossRealClass /= 4
                            lossContainer.discLossRealClass.append(discLossRealClass.item())
                            loss += discLossRealClass
                        loss.backward()

                    latents: torch.Tensor = torch.randn(batchSize, Config.NN.LATENT_CHANNELS, 1, device=device)
                    latentClassIds: torch.Tensor = realClassIds.unsqueeze(1) if useAuxillaryDisc else realClassIds
                    fakeData: torch.Tensor = generator(latents, latentClassIds, stage)

                    fakePredictions: torch.Tensor = None
                    fakeClassifications: torch.Tensor = None
                    # NOTE - `detach` required here so as not to mess up gradients
                    if useAuxillaryDisc:
                        fakePredictions, fakeClassifications = discriminator(fakeData.detach(), stage)
                    else:
                        fakePredictions = discriminator(fakeData.detach(), latentClassIds, stage)
                    discLossFakeClass: torch.Tensor = None
                    if fakeClassifications is not None:
                        discLossFakeClass: torch.Tensor = classificationLoss(fakeClassifications, latentClassIds.squeeze())
                    if useWassersteinLoss:
                        discLossCritic: torch.Tensor = -(realPredictions - fakePredictions).mean()
                        lossContainer.discLossPred.append(-discLossCritic.item())
                        loss: torch.Tensor = discLossCritic

                        gradPenaltyClassIds: torch.Tensor = realClassIds if not useAuxillaryDisc else None
                        discLossGradient: float = loss_ops.WGANGPGradientPenalty(realInputs,
                                                                                 fakeData,
                                                                                 discriminator,
                                                                                 Config.NN.GRADIENT_PENALTY_LAMBDA,
                                                                                 stage,
                                                                                 classIds=gradPenaltyClassIds)
                        lossContainer.discLossGrad.append(discLossGradient)
                        loss += discLossGradient
                        discLossEps: torch.Tensor = (realPredictions**2).mean() * Config.NN.EPSILON_PENALTY_LAMBDA
                        lossContainer.discLossEps.append(discLossEps.item())
                        loss += discLossEps
                        if discLossRealClass is not None and discLossFakeClass is not None:
                            loss += discLossRealClass
                            loss += discLossFakeClass
                            lossContainer.discLossRealClass.append(discLossRealClass.item())
                            lossContainer.discLossFakeClass.append(discLossFakeClass.item())
                        loss.backward()
                    else:
                        discLossFakePred: torch.Tensor = veracityLoss(fakePredictions, class_id_util.generateFakeLabels(batchSize, device)) / 4
                        discLossFakeClass /= 4
                        lossContainer.discLossFakePred.append(discLossFakePred.item())

                        loss: torch.Tensor = discLossFakePred
                        if discLossFakeClass is not None:
                            loss += discLossFakeClass
                            lossContainer.discLossFakeClass.append(discLossFakeClass.item())
                        loss.backward()

                    discOptimizer.step()

                    #handle fake data
                    genOptimizer.zero_grad()
                    # allow updated discriminator to predict data again. NOTE - DO NOT detach here
                    if useAuxillaryDisc:
                        fakePredictions, fakeClassifications = discriminator(fakeData, stage)
                    else:
                        fakePredictions = discriminator(fakeData, latentClassIds, stage)
                    genLossClass: torch.Tensor = None
                    if fakeClassifications is not None:
                        genLossClass = classificationLoss(fakeClassifications, latentClassIds.squeeze())
                    if useWassersteinLoss:
                        genLossPred: torch.Tensor = (-fakePredictions).mean()
                        lossContainer.genLossPred.append(-genLossPred.item())
                        loss: torch.Tensor = genLossPred
                        if genLossClass is not None:
                            lossContainer.genLossClass.append(genLossClass.item())
                            loss += genLossClass
                        loss.backward()
                    else:
                        genLossPred: torch.Tensor = veracityLoss(fakePredictions, class_id_util.generateRealLabels(batchSize, device, False)) / 2
                        lossContainer.genLossPred.append(genLossPred.item())
                        loss: torch.Tensor = genLossPred
                        if genLossClass is not None:
                            genLossClass /= 2
                            lossContainer.genLossClass.append(genLossClass.item())
                            loss += genLossClass
                        loss.backward()

                    genOptimizer.step()

                    if iterations % Config.NN.LOG_FREQUENCY == 0:
                        logAverageLosses(stage, epoch, iterations, lossContainer)

            if epoch % Config.NN.SAMPLE_FREQUENCY == 0:
                Logging.log(f"Saving samples from epoch {epoch}")
                with torch.no_grad():
                    image_io.savePredictions(generator(testLatents, testClassIds, stage), testClassIds, runId, stage, epoch)
                    randomLatents: torch.Tensor = torch.randn(testLatents.shape[0], Config.NN.LATENT_CHANNELS, 1, device=device)
                    image_io.savePredictions(generator(randomLatents, testClassIds, stage), testClassIds, runId, stage, epoch, randomInputs=True)

            Logging.log(f"Finished stage {stage} epoch {epoch}")
            logAverageLosses(stage, epoch, iterations, lossContainer)

            if epoch % Config.NN.SAVE_FREQUENCY == 0:
                Logging.log(f"Saving model from epoch {epoch}")
                timesSaved += 0.25
                halfwayStage: float = stage + timesSaved
                model_io.saveGeneratorAndOptimizer(generator, genOptimizer, halfwayStage, runId)
                model_io.saveDiscriminatorAndOptimizer(discriminator, discOptimizer, halfwayStage, runId)

            epoch += 1
        Logging.log(f"Finished stage {stage}")
        model_io.saveGeneratorAndOptimizer(generator, genOptimizer, stage, runId)
        model_io.saveDiscriminatorAndOptimizer(discriminator, discOptimizer, stage, runId)
        stage += 1

    Logging.log("Finished training. Check logs at " + str(Logging.LOG_FILE))


def logAverageLosses(stage: int, epoch: int, iterations: int, lossContainer: LossContainer) -> None:
    Logging.log(f"Stage {stage}, epoch {epoch}, iter {iterations}. {lossContainer.generateLogString()}")


def main(runId: str):
    Logging.LOG_FILE = Logging.generateLogFilePath(runId)
    try:
        args = docopt(__doc__)
        clArgs = CLArgs(args)
        device = clArgs.getDevice()
        savedModelPath: str = None
        stage: int = 0
        if clArgs.isResumeRun():
            savedModelPath = "2019_11_20_16_54_59_re_re_re_re_re_final_s4"
            # savedModelPath = "2019_11_20_16_54_59_re_re_re_re_s3"
            # savedModelPath = "2019_11_20_16_54_59_re_re_re_re_s4"
            # savedModelPath = "2019_11_20_16_54_59_re_re_re_re_re_s4"

            runId = savedModelPath[:-3] + "_re"
            stage = int(savedModelPath[-1])
            if clArgs.startNextStage():
                stage += 1
                Logging.log(f"Starting training at stage {stage}")
        elif clArgs.isSampleRun():
            savedModelPath = "2019_11_20_16_54_59_re_re_re_re_s3"
            stage = int(savedModelPath[-1])

        Logging.log(f"Commencing run {runId}")
        Logging.log("Using device " + str(device))
        Logging.log("GPUs detected: " + str(torch.cuda.device_count()))
        if clArgs.isSampleRun():
            Logging.log(f"Initializing stage {stage} sample run of model {savedModelPath}")
            sample(runId, savedModelPath, Config.NN.SAMPLE_SIZE, stage, device)
        elif clArgs.trainFID():
            fid_classifier.trainFidClassifier(runId, 4, device)
        elif clArgs.computeFID():
            fid_classifier.computeFID("2019_12_01_22_47_27_s4_FID",
                                        "./data/size32",
                                        "./samples/2019_12_05_12_37_05",
                                        Config.NN.BATCH_SIZE,
                                        device,
                                        Config.NN.INCEPTION_OUTPUT_SIZE)
        else:
            train(runId, device, stage, savedModelPath=savedModelPath, loadNewOptimizers=clArgs.startNextStage() or clArgs.forceNewOptimizers(), overrideAlpha=clArgs.overrideAlpha())

    except Exception as e:
        Logging.log("Run failed due to exception, please check logs")
        Logging.log(e)
        raise e

if __name__ == '__main__':
    timestamp = time.time()
    runId = datetime.datetime.fromtimestamp(timestamp).strftime('%Y_%m_%d_%H_%M_%S')
    torch.manual_seed(Config.NN.RANDOM_SEED)
    main(runId)




