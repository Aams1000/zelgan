import torch
import torch.nn as nn
from models.convleakypixel import ConvLeakyPixel
from models.rescaling_linear import RescalingLinear
import models.weightscaling as weightscaling


class InitialGeneratorBlock(nn.Module):

    def __init__(self,
                 inFeatures: int,
                 outFeatures: int,
                 channels: int,
                 kernelSize: int = 3,
                 padding: int = 1,
                 stride: int = 1):
        super(InitialGeneratorBlock, self).__init__()
        self.rescalingLinear: RescalingLinear = RescalingLinear(inFeatures, outFeatures, usePixelNorm=True, shouldReshapeAfterLinear=True)
        self.clp: ConvLeakyPixel = ConvLeakyPixel(channels, channels, kernelSize, padding, stride)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        x = weightscaling.pixelNorm(x)
        x = self.rescalingLinear(x)
        x = self.clp(x)
        return x


if __name__ == "__main__":
    testInput: torch.Tensor = torch.rand((3, 256, 1))
    testBlock: InitialGeneratorBlock = InitialGeneratorBlock(1, 36, 256)
    assert(testBlock(testInput).shape == (3, 256, 6, 6))



