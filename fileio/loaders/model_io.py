import torch
from typing import Dict
from fileio.customlogging import Logging
from typing import Tuple


__CHECKPOINT_DIR: str = "./checkpoints/"


class ObjectType:
    MODEL: str = ".model"
    OPTIMIZER: str = ".optim"


class ModelType:
    GENERATOR: str = "G"
    DISCRIMINATOR: str = "D"
    FID: str = "FID"


def saveGeneratorAndOptimizer(generator: torch.nn.Module, optimizer: torch.optim.Optimizer, stage: float, runId: str) -> None:
    __saveModelAndOptimizer(generator, optimizer, stage, runId, ModelType.GENERATOR)


def saveDiscriminatorAndOptimizer(discriminator: torch.nn.Module, optimizer: torch.optim.Optimizer, stage: float, runId: str) -> None:
    __saveModelAndOptimizer(discriminator, optimizer, stage, runId, ModelType.DISCRIMINATOR)


def __saveModelAndOptimizer(model: torch.nn.Module, optimizer: torch.optim.Optimizer, stage: float, runId: str, modelType: str) -> None:
    __saveStateDict(model.state_dict(), modelType, stage, runId, ObjectType.MODEL)
    __saveStateDict(optimizer.state_dict(), modelType, stage, runId, ObjectType.OPTIMIZER)


def saveModelStateDict(stateDict: Dict, modelType: str, stage: float, runId: str) -> None:
    __saveStateDict(stateDict, modelType, stage, runId, ObjectType.MODEL)


def saveOptimizerStateDict(stateDict: Dict, modelType: str, stage: float, runId: str) -> None:
    __saveStateDict(stateDict, modelType, stage, runId, ObjectType.OPTIMIZER)


def __saveStateDict(stateDict: Dict, modelType: str, stage: float, runId: str, objectTypeSuffix: str) -> None:
    path = __CHECKPOINT_DIR + f"{runId}_s{stage}_{modelType}{objectTypeSuffix}"
    torch.save(stateDict, path)
    Logging.log(f"Saved {modelType} state dict to {path}")


def saveInceptionClassifierAndOptimizer(modelStateDict: Dict,
                                        optimizerStateDict: Dict,
                                        stage: float,
                                        runId: str) -> None:
    __saveStateDict(modelStateDict, ModelType.FID, stage, runId, ObjectType.MODEL)
    __saveStateDict(optimizerStateDict, ModelType.FID, stage, runId, ObjectType.MODEL)


def loadInceptionClassifier(fileName: str, device: torch.device = None) -> Dict:
    return __loadStateDict(fileName, ObjectType.MODEL, device)


def loadGeneratorSaveDict(fileName: str, device: torch.device = None) -> Dict:
    return __loadStateDict(fileName + f"_{ModelType.GENERATOR}", ObjectType.MODEL, device)


def loadDiscriminatorSaveDict(fileName: str, device: torch.device = None) -> Dict:
    return __loadStateDict(fileName + f"_{ModelType.DISCRIMINATOR}", ObjectType.MODEL, device)


def loadGeneratorAndOptimizerStateDicts(fileName: str, device: torch.device = None) -> Tuple[Dict, Dict]:
    return __loadModelAndOptimizerStateDicts(fileName + f"_{ModelType.GENERATOR}", device)


def loadGeneratorOptimizerStateDict(fileName: str, device: torch.device = None) -> Dict:
    return __loadStateDict(fileName + f"_{ModelType.GENERATOR}", ObjectType.OPTIMIZER, device)


def loadDiscriminatorOptimizerStateDict(fileName: str, device: torch.device = None) -> Dict:
    return __loadStateDict(fileName + f"_{ModelType.DISCRIMINATOR}", ObjectType.OPTIMIZER, device)


def loadDiscriminatorAndOptimizerStateDicts(fileName: str, device: torch.device = None) -> Tuple[Dict, Dict]:
    return __loadModelAndOptimizerStateDicts(fileName + f"_{ModelType.DISCRIMINATOR}", device)


def __loadModelAndOptimizerStateDicts(fileName: str, device: torch.device = None) -> Tuple[Dict, Dict]:
    modelStateDict: Dict = __loadStateDict(fileName, ObjectType.MODEL, device)
    optStateDict: Dict = __loadStateDict(fileName, ObjectType.OPTIMIZER, device)
    return modelStateDict, optStateDict


def __loadStateDict(fileName: str, objectTypeSuffix: str, device: torch.device = None) -> Dict:
    if device is not None:
        return torch.load(__CHECKPOINT_DIR + fileName + objectTypeSuffix, map_location=device)
    else:
        return torch.load(__CHECKPOINT_DIR + fileName + objectTypeSuffix)
