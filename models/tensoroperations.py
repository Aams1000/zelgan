import torch


def flatten(x: torch.Tensor) -> torch.Tensor:
    secondDim: int = 1
    for i in range(1, len(x.shape)):
        secondDim *= x.shape[i]
    return x.reshape(x.shape[0], secondDim)


if __name__ == "__main__":
    torch.manual_seed(5)
    testInput: torch.Tensor = torch.rand((4, 32, 8, 8))
    assert(flatten(testInput).shape == (4, 32 * 8 * 8))
