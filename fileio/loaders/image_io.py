from typing import *
from PIL import Image, ImageOps
import numpy as np
import torch
import torchvision
import datalabels.class_ids as classIdutil
import os


__DATA_DIR: str = "./data/"
__SAMPLES_DIR: str = "./samples/"


def __getDataDirForStage(stage: int, isFIDValidation: bool) -> str:
    valPrefix: str = "val_" if isFIDValidation else ""
    return __DATA_DIR + valPrefix + "size" + str(2**(stage + 2)) + "/"


def __getImageFileNames(stage: int, isFIDValidation: bool) -> List[str]:
    relativePathPrefix: str = __getDataDirForStage(stage, isFIDValidation)
    ids = [relativePathPrefix + fileName for fileName in os.listdir(relativePathPrefix)]
    return ids


def getIdToFileNameMap(stage: int, isFIDValidation: bool) -> Dict[int, str]:
    fileNames: List[str] = __getImageFileNames(stage, isFIDValidation)
    idToFileName: Dict[int, str] = {}
    for i, fileName in enumerate(fileNames):
        idToFileName[i] = fileName
    return idToFileName


def loadImage(fileName: str, desiredSize: Tuple[int, int]) -> torch.Tensor:
    image = Image.open(fileName)
    if desiredSize is not None:
        image = __cropOrPadImage(image, desiredSize)
    # image.show()
    # input("test")
    # raise ValueError()
    npArray = np.asarray(image)
    # print(npArray)
    # Image.fromarray(npArray)
    image = torch.from_numpy(npArray).float()
    # print(image.shape)
    image = image.permute((2, 0, 1))
    # print(image.shape)
    return image


def __cropOrPadImage(image: Image.Image, heightAndWidth: Tuple[int, int]) -> Image.Image:
    originalSize = image.size
    targetHeight, targetWidth = heightAndWidth
    image.thumbnail(heightAndWidth)
    croppedHeight, croppedWidth = image.size
    heightDifference = targetHeight - croppedHeight
    widthDifference = targetWidth - croppedWidth
    paddingValues = None
    if widthDifference > 0 or heightDifference > 0:
        halfWidthDifferenceFloor = int(widthDifference / 2)
        halfHeightDifferenceFloor = int(heightDifference / 2)
        if widthDifference > 0 and heightDifference > 0:
            paddingValues = (halfHeightDifferenceFloor,
                             halfWidthDifferenceFloor,
                             heightDifference - halfHeightDifferenceFloor,
                             widthDifference - halfWidthDifferenceFloor)
        elif widthDifference > 0:
            paddingValues = (0,
                             halfWidthDifferenceFloor,
                             0,
                             widthDifference - halfWidthDifferenceFloor)
        else:
            paddingValues = (halfHeightDifferenceFloor,
                             0,
                             heightDifference - halfHeightDifferenceFloor,
                             0)
    if paddingValues is not None:
        image = ImageOps.expand(image, paddingValues)
    return image


def savePredictions(predictions: torch.Tensor,
                    classIds: torch.Tensor,
                    runId: str,
                    stage: int,
                    epoch: int,
                    randomInputs: bool = False) -> None:
    for i in range(0, predictions.shape[0]):
        classId: str = classIdutil.getStringNameForClassId(classIds[i].item())
        filePath: str = createFilePathForImage(runId, stage, epoch, randomInputs, classId, i)
        # print(predictions[i].squeeze().shape)
        # Image.fromarray(predictions[i].permute(2, 0, 1).data)
        Image.fromarray(predictions[i].squeeze().permute(1, 2, 0).cpu().numpy().astype(np.uint8)).save(filePath)
        # Image.fromarray(predictions[i].squeeze().permute(2, 0, 1).data).save(filePath)
        # raise ValueError()
        # torchvision.utils.save_image(predictions[i].squeeze(), filePath)


# def saveGridPredictions(predictions: torch.Tensor,
#                         id: int,
#                         runId: str,
#                         stage: int,
#                         epoch: int,
#                         iteration: int) -> None:
#     for i in range(0, predictions.shape[0]):
#         classId: str = classIdutil.getStringNameForClassId(id)
#         filePath: str = createFilePathForImage(runId, stage, epoch, iteration, classId, i)
#         # torchvision.utils.make_grid(predictions,)
#         # print(predictions[i].squeeze().shape)
#         # Image.fromarray(predictions[i].permute(2, 0, 1).data)
#         # Image.fromarray(predictions[i].squeeze().permute(1, 2, 0).cpu().numpy().astype(np.uint8)).save(filePath)
#         # Image.fromarray(predictions[i].squeeze().permute(2, 0, 1).data).save(filePath)
#         # raise ValueError()
#         torchvision.utils.save_image(predictions, filePath)


def createFilePathForImage(runId: str,
                           stage: int,
                           epoch: int,
                           randomInputs: bool,
                           classId: str,
                           index: int) -> str:
    sampleDirectory: str = __SAMPLES_DIR + f"/{runId}/"
    os.makedirs(sampleDirectory, exist_ok=True)
    randomKey: int = 1 if randomInputs else 0
    return sampleDirectory + f"s{stage}_e{epoch}_r{randomKey}_k{index}_{classId}.jpg"



if __name__ == "__main__":
    fileNames: List[str] = __getImageFileNames(6)
    fileNameToIdMap: Dict[int, str] = getIdToFileNameMap(6)
    print(fileNameToIdMap)
