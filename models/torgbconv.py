import torch
import torch.nn as nn
import models.weightscaling as weightscaling


class ToRGBConv(nn.Module):

    def __init__(self,
                 inChannels: int):
        super(ToRGBConv, self).__init__()
        outChannels: int = 3
        self.conv: nn.Conv2d = nn.Conv2d(inChannels, outChannels, kernel_size=1)
        nn.init.normal_(self.conv.weight)
        torch.nn.init.zeros_(self.conv.bias)
        self.weightScale: float = weightscaling.computeWeightScale(self.conv)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        #TODO - use no_grad? is this right?
        x = self.conv(x) * self.weightScale
        return x

if __name__ == "__main__":
    testInput: torch.Tensor = torch.rand((1, 311, 2, 2))
    assert(ToRGBConv(311)(testInput).shape == (1, 3, 2, 2))