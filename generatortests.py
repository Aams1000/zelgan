import unittest
from models.generator import Generator
import torch


class GeneratorTests(unittest.TestCase):

    def testOutputShapesWithLabels(self):
        testInput: torch.Tensor = torch.rand((2, 496, 1))
        testLabels: torch.Tensor = torch.ones((2, 1)).long()
        testGenerator: Generator = Generator(496, 1, 4, 10)
        for i in range(0, 7):
            assert(testGenerator(testInput, testLabels, i).shape == (2, 3, 2**(i + 2), 2**(i + 2)))

    def testOutputShapesNoLabels(self):
        testInput: torch.Tensor = torch.rand((2, 512, 1))
        testLabels: torch.Tensor = torch.ones((2, 1)).long()
        testGenerator: Generator = Generator(496, 1, 4, 10)
        for i in range(0, 7):
            assert(testGenerator(testInput, testLabels, i, False).shape == (2, 3, 2**(i + 2), 2**(i + 2)))

    def testAlphaIncrementation(self):
        testInput: torch.Tensor = torch.rand((2, 496, 1))
        testLabels: torch.Tensor = torch.ones((2, 1)).long()
        testGenerator: Generator = Generator(496, 1, 4, 15)
        testGenerator(testInput, testLabels, 4)
        testGenerator(testInput, testLabels, 6)
        testGenerator(testInput, testLabels, 6)
        for i in range(0, len(testGenerator.alphas)):
            targetValue: float = testGenerator.alphaIncrement
            if i == 4 or i == 6:
                targetValue *= (i - 1)
                self.assertEqual(testGenerator.alphas[i], targetValue)

    def testAlphaCombination(self):
        torch.manual_seed(5)
        testInput: torch.Tensor = torch.rand((2, 64, 1))
        testLabels: torch.Tensor = torch.ones((2, 1)).long()
        testGenerator: Generator = Generator(48, 1, 4, 10, alphaIncrement=0.5)
        previousOutput: torch.Tensor = torch.FloatTensor([[-0.6796, -0.6796, -1.0325, -1.0325,  0.0774,  0.0774, -2.2369,
                                                           -2.2369],
                                                          [-0.6796, -0.6796, -1.0325, -1.0325,  0.0774,  0.0774, -2.2369,
                                                           -2.2369],
                                                          [-0.9837, -0.9837, -1.6598, -1.6598,  0.7535,  0.7535, -1.8864,
                                                           -1.8864],
                                                          [-0.9837, -0.9837, -1.6598, -1.6598,  0.7535,  0.7535, -1.8864,
                                                           -1.8864],
                                                          [ 1.1916,  1.1916,  0.1339,  0.1339, -0.6703, -0.6703,  1.2056,
                                                            1.2056],
                                                          [ 1.1916,  1.1916,  0.1339,  0.1339, -0.6703, -0.6703,  1.2056,
                                                            1.2056],
                                                          [ 0.2912,  0.2912,  0.8145,  0.8145, -1.4044, -1.4044,  0.5903,
                                                            0.5903],
                                                          [ 0.2912,  0.2912,  0.8145,  0.8145, -1.4044, -1.4044,  0.5903,
                                                            0.5903]])

        nextLayerOutput: torch.Tensor = torch.FloatTensor([[ 0.5803,  0.1652, -0.2403, -0.3205,  0.7422,  0.4569,  0.1743, -0.3636],
                                                           [ 0.9957, -0.3736, -0.3978, -0.7026, -0.7726, -0.3540, -0.3543, -1.5488],
                                                           [-0.3606,  0.3295, -0.1590,  0.2780,  0.3493, -0.0309,  0.3746, -1.9633],
                                                           [-1.2314, -0.2432,  0.1652,  0.5649,  0.3395,  0.0694,  0.9709, -0.4674],
                                                           [ 0.5102,  1.1644,  0.4640,  0.1768,  0.0123,  0.4648,  1.1283, -0.0279],
                                                           [ 0.7849, -0.0429, -0.1199, -0.1042,  0.3600,  0.5876,  0.0787, -0.5424],
                                                           [ 0.4478, -0.2756, -0.7535, -0.1468,  0.2222,  0.8917,  1.0681, -1.4616],
                                                           [ 1.2730,  0.9166,  0.5491, -0.3588, -1.5494, -0.8291,  0.7305, -0.5527]])

        expectedResult: torch.Tensor = torch.add((1 - testGenerator.alphaIncrement) * previousOutput, testGenerator.alphaIncrement * nextLayerOutput)
        result: torch.Tensor = testGenerator(testInput, testLabels, 1, False)[0, 0]
        self.assertAlmostEqual(torch.sum(expectedResult), torch.sum(result), delta=1e-2)

    def testAlphaThreshold(self):
        torch.manual_seed(5)
        testInput: torch.Tensor = torch.rand((2, 64, 1))
        testLabels: torch.Tensor = torch.ones((2, 1)).long()
        testGenerator: Generator = Generator(48, 1, 4, 10)
        testGenerator.alphas[2] += 1
        expectedOutput: torch.Tensor = torch.FloatTensor([[ 0.6101, -0.8545, -2.1522, -1.5304, -1.6057, -0.8843, -0.8295,  0.3784,
                                                            0.7848,  1.6226,  0.6628, -0.5354, -0.8040, -1.7987, -2.6456, -1.8696],
                                                          [-0.0596, -0.0184, -0.0618, -0.7910, -1.1039, -0.6751, -1.3019,  0.1837,
                                                           0.0391,  0.3380, -0.1818, -0.4722, -1.2420, -1.7094, -1.9464, -1.6965],
                                                          [ 0.3071, -0.0998, -0.7299, -1.6915, -1.8644, -1.6961, -1.7771, -1.0880,
                                                            -1.4959, -0.5394, -1.2059, -1.5314, -1.8460, -2.1966, -2.4343, -2.0084],
                                                          [ 1.2606,  0.4670, -0.4432, -2.0373, -2.0625, -1.4119, -1.4836, -1.4011,
                                                            -1.8030, -0.5051, -1.7984, -2.1888, -1.6238, -2.2467, -2.1138, -1.3494],
                                                          [ 1.2666, -0.1962, -0.5742, -1.9174, -2.2288, -1.4726, -0.8429, -0.7114,
                                                            -0.7708, -0.8318, -1.2387, -1.4535, -1.9268, -2.2253, -1.9897, -1.7114],
                                                          [ 0.2103, -1.0246, -0.8532, -2.2311, -2.2124, -1.5390, -0.1951,  0.5742,
                                                            -0.0130, -0.7025, -1.1799, -0.9122, -1.9151, -1.8144, -1.6300, -1.8529],
                                                          [ 0.4603, -1.4466, -1.5163, -2.2568, -2.1762, -1.1105, -0.3841,  0.3724,
                                                            -0.3843, -1.3073, -1.1626, -1.0607, -2.2244, -2.4295, -1.4573, -1.7434],
                                                          [ 0.2321, -1.0861, -1.6714, -2.0823, -2.0349, -1.0823, -0.9759, -0.5913,
                                                            -1.9798, -2.2789, -1.5447, -1.9458, -2.3793, -2.3203, -1.6217, -1.9162],
                                                          [ 0.1874, -0.6314, -2.5252, -2.2726, -1.6386, -1.7812, -1.2850, -1.0735,
                                                            -3.1167, -3.1588, -1.6822, -1.9163, -2.0227, -1.8325, -2.2307, -2.0354],
                                                          [ 1.5047, -0.2561, -2.2715, -1.8879, -1.4135, -1.4254, -1.3049, -1.0557,
                                                            -2.0747, -2.6416, -1.6017, -2.0648, -1.1667, -0.4209, -1.7645, -1.8013],
                                                          [ 0.9956, -0.4681, -1.7745, -1.8052, -1.2535, -0.8818, -1.1301, -0.5408,
                                                            -1.8478, -2.2979, -1.3868, -2.0837, -1.0442, -0.0281, -1.6494, -1.5152],
                                                          [-0.6503, -1.4928, -2.0087, -1.6021, -0.9029, -0.6541, -0.6886, -0.6013,
                                                           -1.7639, -2.7276, -1.6823, -1.9136, -0.8672,  0.3883, -0.8902, -1.3890],
                                                          [-0.2078, -0.5982, -1.0934, -1.5272, -0.6260, -0.3758, -0.2048, -0.7684,
                                                           -1.2626, -1.8809, -1.4096, -1.3610, -0.3978,  0.5558, -0.0706, -0.5250],
                                                          [ 0.5774, -0.4171, -0.8701, -1.0955, -0.3637,  0.1459, -0.7364, -1.0045,
                                                            -1.2018, -1.7785, -1.0150, -1.3962, -0.9664, -0.4968,  0.3492, -0.6242],
                                                          [-0.0234, -1.0678, -0.9395, -1.7092, -1.2754, -0.8998, -0.8699, -0.5564,
                                                           -0.5401, -1.7758, -1.6037, -1.2040, -1.1705, -0.1871,  1.0669, -1.8077],
                                                          [ 0.7402,  0.1347, -1.5959, -1.5516, -0.9365, -0.6618,  0.0654, -0.5567,
                                                            -1.0105, -0.7269, -1.0016, -0.3099, -0.4662,  0.2478,  1.2199,  0.2566]])
        result: torch.Tensor = testGenerator(testInput, testLabels, 2, False)[0, 0]
        self.assertAlmostEqual(torch.sum(expectedOutput), torch.sum(result), delta=1e-2)

if __name__ == '__main__':
    unittest.main()
