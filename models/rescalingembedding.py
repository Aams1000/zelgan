import torch
import torch.nn as nn
import models.weightscaling as weightscaling
from typing import Tuple


class RescalingEmbedding(nn.Module):

    def __init__(self,
                 numLabels: int,
                 labelEmbeddingSize: int,
                 scaleRange: Tuple[float, float] = None):
        super(RescalingEmbedding, self).__init__()
        self.scaleRange: Tuple[float, float] = scaleRange
        self.embedding: nn.Embedding = nn.Embedding(numLabels, labelEmbeddingSize)
        self.weightScale: float = weightscaling.computeWeightScale(self.embedding)

    def forward(self, x: int) -> torch.Tensor:
        x = self.embedding(x) * self.weightScale
        # print(f"Embeddings: {x.shape}")
        if self.scaleRange is not None:
            x = weightscaling.scaleTensorToRange(x, self.scaleRange)
        return x


if __name__ == "__main__":
    torch.manual_seed(5)
    testLayer: RescalingEmbedding = RescalingEmbedding(5, 16)
    assert(testLayer(torch.ones(size=(1, 1)).long()).shape == (1, 1, 16))

    scaleRange: Tuple[float, float] = (0, 255)
    testLayer: RescalingEmbedding = RescalingEmbedding(5, 16, scaleRange)
    output: torch.Tensor = testLayer(torch.ones((5, 1)).long())
    assert(torch.min(output) >= scaleRange[0] and torch.max(output) <= scaleRange[1])

    scaleRange: Tuple[float, float] = (-1.25, 1.25)
    testLayer: RescalingEmbedding = RescalingEmbedding(5, 16, scaleRange)
    output: torch.Tensor = testLayer(torch.ones((5, 1)).long())
    assert(torch.min(output) >= scaleRange[0] and torch.max(output) <= scaleRange[1])



