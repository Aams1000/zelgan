from typing import *
import re
from fileio.customlogging import Logging
from fileio.loaders import image_io
import torch
import models.weightscaling as weight_scaling


__PREFIX_TO_ID_MAP: Dict[str, int] = {"legzelda3": 0,
                                      "smetroid": 1,
                                      "Yisland": 2,
                                      "pokemongold": 3,
                                      "supmario3": 4,
                                      "mariolost": 5,
                                      "metroid": 6,
                                      "chronotrig": 7,
                                      "mario1": 8,
                                      "pokemonred": 9,
                                      "legendzelda": 10,
                                      "supmario2": 11,
                                      "castlev4": 12,
                                      "smw": 13}
__ID_TO_PREFIX_MAP: Dict[int, str] = {id: name for name, id in __PREFIX_TO_ID_MAP.items()}

__FILE_NAME_PATTERN: str = "(?:(?!/).)+?(?=-)"

__REAL_VALUE_RANGE: Tuple[float, float] = (0.7, 1.0)
__FAKE_VALUE_RANGE: Tuple[float, float] = (0.0, 0.3)
__FLIP_PROBABILITY: float = 0.05


def getClassIdForImage(fileName: str) -> int:
    try:
        prefix: str = re.search(__FILE_NAME_PATTERN, fileName).group(0)
        return __PREFIX_TO_ID_MAP[prefix]
    except AttributeError as e:
        Logging.log(f"File name {fileName} does not match any IDs.")
        raise e


def getNumClassIds() -> int:
    return len(__PREFIX_TO_ID_MAP)


def generateRealLabels(batchSize: int, device: torch.device, shouldUseNoise: bool = True, flipProbability: float = __FLIP_PROBABILITY) -> torch.Tensor:
    if shouldUseNoise:
        return __generateNoisyCombination(batchSize, device, __REAL_VALUE_RANGE, __FAKE_VALUE_RANGE, flipProbability)
    else:
        return torch.ones((batchSize, 1), device=device)


def __generateNoisyCombination(batchSize: int,
                               device: torch.device,
                               commonRange: Tuple[float, float],
                               rareRange: Tuple[float, float],
                               flipProbability: float) -> torch.Tensor:
    commonMask: torch.Tensor = (torch.rand(batchSize, 1, device=device) > flipProbability)
    rareMask: torch.Tensor = torch.zeros_like(commonMask, device=device)
    rareMask[commonMask != 1] = 1
    return commonMask.float() * __generateNoisyLabels(batchSize, device, commonRange) + rareMask.float() * __generateNoisyLabels(batchSize, device, rareRange)


def __generateNoisyLabels(batchSize: int, device: torch.device, range: Tuple[float, float]) -> torch.Tensor:
    return torch.FloatTensor(batchSize, 1, device=device).uniform_(range[0], range[1])


def generateFakeLabels(batchSize: int, device: torch.device, flipProbability: float = __FLIP_PROBABILITY) -> torch.Tensor:
    return __generateNoisyCombination(batchSize, device, __FAKE_VALUE_RANGE, __REAL_VALUE_RANGE, flipProbability)


def getStringNameForClassId(id: int) -> str:
    return __ID_TO_PREFIX_MAP[id]

if __name__ == "__main__":
    fileNames: List[str] = image_io.__getImageFileNames()
    for fileName in fileNames:
        print(fileName, getClassIdForImage(fileName))

