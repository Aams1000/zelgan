import torch
import torch.nn as nn
from models.initialgeneratorblock import InitialGeneratorBlock
from models.torgbconv import ToRGBConv
import models.generator_operations as gen_ops
from typing import Tuple, Dict
from models.rescalingembedding import RescalingEmbedding


class Generator(nn.Module):

    __CLASS_ID_RESCALING_FACTOR: Tuple[float, float] = (-1.0, 1.0)

    def __init__(self,
                 latentChannels: int,
                 latentFeatures: int,
                 initialResolution: int,
                 numClassIds: int,
                 classIdEmbeddingSize: int = 16,
                 channelDecreasingRate: int = 2,
                 numMainBlocks: int = 6,
                 alphaIncrement: float = 0.000125,
                 scaleFactor: float = 2.0,
                 initialStage: int = 0):
        super(Generator, self).__init__()
        numInitialChannels: int = latentChannels + classIdEmbeddingSize
        self.generatorBlocks: nn.ModuleList = nn.ModuleList([InitialGeneratorBlock(latentFeatures, initialResolution**2, numInitialChannels)])
        self.rgbBlocks: nn.ModuleList = nn.ModuleList([ToRGBConv(numInitialChannels)])
        mainBlocksAndRGBBlocks: Tuple[nn.ModuleList, nn.ModuleList] = gen_ops.constructMainBlocksAndRGBs(numInitialChannels, channelDecreasingRate, numMainBlocks)
        self.generatorBlocks.extend(mainBlocksAndRGBBlocks[0])
        self.rgbBlocks.extend(mainBlocksAndRGBBlocks[1])
        self.scaleFactor: float = scaleFactor
        self.alphaIncrement: float = alphaIncrement

        self.alphas: Dict[int, float] = gen_ops.initializeAlphas(initialStage, alphaIncrement, numMainBlocks)
        print(self.alphas)
        # deliberately putting this after the weight initialization so as not to break the tests
        # using a random seed
        self.classIdEmbedding: RescalingEmbedding = RescalingEmbedding(numClassIds, classIdEmbeddingSize, Generator.__CLASS_ID_RESCALING_FACTOR)

    def forward(self, x: torch.Tensor, classIds: torch.Tensor, stage: int, useClassIds: bool = True) -> torch.Tensor:
        if useClassIds:
            # print(f"Raw x: {x.shape}")
            classIds = self.classIdEmbedding(classIds).permute(0, 2, 1)
            # print(f"Embedded labels: {labels.shape}")
            x = torch.cat((x, classIds), dim=1)
            # print(f"Concatenated labels: {x.shape}")
        if stage == 0:
            x = gen_ops.blockPlusRGB(x, stage, self.generatorBlocks, self.rgbBlocks)
        else:
            for i in range(0, stage):
                # print(f"Executing block {i}...")
                x = self.generatorBlocks[i](x)
            alpha: float = self.alphas[stage]
            # print(alpha)
            if alpha < 1.0:
                # print("Scaling...")
                x = gen_ops.computeWeightedOutput(x, stage, alpha, self.scaleFactor, self.generatorBlocks, self.rgbBlocks)
                gen_ops.updateAlpha(self.alphas, stage, x.shape[0], self.alphaIncrement)
            else:
                # print(f"Not scaling layer {stage}")
                x = gen_ops.blockPlusRGB(x, stage, self.generatorBlocks, self.rgbBlocks)

        return x




