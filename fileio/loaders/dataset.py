import torch
from torch.utils import data
from fileio.loaders import image_io
import datalabels.class_ids as labels
from typing import *
from fileio.customlogging import Logging
from PIL import Image
import numpy as np


class Dataset(data.Dataset):

    def __init__(self, stage: int, desiredDimension: Tuple[int, int] = None, isFIDValidation: bool = False):
        super(Dataset, self).__init__()
        self.idToFileNameMap: Dict[int, str] = image_io.getIdToFileNameMap(stage, isFIDValidation)
        self.desiredDimension: Tuple[int, int] = desiredDimension

    def __len__(self):
        return len(self.idToFileNameMap)

    def __getitem__(self, index) -> Tuple[torch.Tensor, int]:
        fileName: str = self.idToFileNameMap[index]
        # Logging.log(fileName)
        label = labels.getClassIdForImage(fileName)
        imageTensor = image_io.loadImage(fileName, self.desiredDimension)
        # print(imageTensor.shape)
        # print(imageTensor.permute(1, 2, 0).numpy().shape)
        # print(imageTensor.permute(1, 2, 0).numpy().astype(np.uint8))
        # Image.fromarray(imageTensor.permute(1, 2, 0).numpy().astype(np.uint8)).show()
        # raise ValueError()
        return imageTensor, label

