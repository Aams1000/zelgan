import torch


# This is the funtion from the Facebook version of the GAN: https://github.com/facebookresearch/pytorch_GAN_zoo/blob/master/models/networks/mini_batch_stddev_module.py
# My code produces slightly different results--specifically, their line torch.var(y, 1) produces different numbers from mine
# I find it truly incomprehensible why this is happening, but I will return to it later if I have time
def __facebookMiniBatchStdDev(x, subGroupSize=4):
    r"""
    Add a minibatch standard deviation channel to the current layer.
    In other words:
        1) Compute the standard deviation of the feature map over the minibatch
        2) Get the mean, over all pixels and all channels of thsi ValueError
        3) expand the layer and cocatenate it with the input
    Args:
        - x (tensor): previous layer
        - subGroupSize (int): size of the mini-batches on which the standard deviation
        should be computed
    """
    size = x.size()
    subGroupSize = min(size[0], subGroupSize)
    if size[0] % subGroupSize != 0:
        subGroupSize = size[0]
    G = int(size[0] / subGroupSize)
    if subGroupSize > 1:
        y = x.view(-1, subGroupSize, size[1], size[2], size[3])
        # print(y[0, 0, 0])
        y = torch.var(y, 1)
        y = torch.sqrt(y + 1e-8)
        # print(f"Pre reshape: {y.shape}")
        y = y.view(G, -1)
        # print(f"Curr size: {y.shape}")
        y = torch.mean(y, 1).view(G, 1)
        # print(y)
        y = y.expand(G, size[2]*size[3]).view((G, 1, 1, size[2], size[3]))
        y = y.expand(G, subGroupSize, -1, -1, -1)
        y = y.contiguous().view((-1, 1, size[2], size[3]))
        # print(f"Final size: {y.shape}")
        # print(y)
    else:
        y = torch.zeros(x.size(0), 1, x.size(2), x.size(3), device=x.device)

    return torch.cat([x, y], dim=1)

def appendMinibatchStdLayer(x: torch.Tensor, groupSize: int = 4) -> torch.Tensor:
    return __facebookMiniBatchStdDev(x, groupSize)

# my implementation
# def appendMinibatchStdLayer(x: torch.Tensor, groupSize: int = 4) -> torch.Tensor:
#     N, C, H, W = x.shape
#     numGroups: int = N // groupSize
#     stdDev: torch.Tensor = x.reshape((groupSize, numGroups, C, H, W))
#     # print(stdDev[0, 0, 0])
#     # print(stdDev.shape)
#     stdDev -= stdDev.mean(dim=0, keepdim=True)
#     # stdDev =
#     stdDev = stdDev.std(dim=0, unbiased=True)
#
#     # print(stdDev)
#     stdDev = stdDev.mean(dim=(1, 2, 3))
#     # print(stdDev)
#     stdDev = stdDev.unsqueeze(1)
#     # print(stdDev)
#     stdDev = stdDev.expand(numGroups, H * W).reshape(numGroups, 1, 1, H, W)
#     stdDev = stdDev.expand(numGroups, groupSize, 1, H, W).reshape(numGroups * groupSize, 1, H, W)
#     # print(stdDev.shape)
#     # stdLayer: torch.Tensor = x.std(dim=(0, 1), keepdim=True)
#     # print(f"Std layer: {stdDev.shape}")
#     # print(stdDev[:, 0, :, :])
#     return torch.cat((x, stdDev), dim=1)
#     # print(stdDev)
#     # return x

if __name__ == "__main__":
    torch.manual_seed(5)
    testInput: torch.Tensor = torch.rand((16, 16, 2, 2))
    print(torch.sum(testInput - testInput))
    print(__facebookMiniBatchStdDev(testInput).shape)
    print(appendMinibatchStdLayer(testInput).shape)
    difference: torch.Tensor = torch.abs(torch.sum(__facebookMiniBatchStdDev(testInput) - appendMinibatchStdLayer(testInput)))
    print(f"Difference: {difference}")
    assert(difference - 2.5616753101348877 < 1e-8)




