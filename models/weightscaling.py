import torch
import torch.nn as nn
import math
from typing import Tuple
import torch.nn.functional as F


__EPSILON: float = 1e-8


# NOTE - this is NOT a norm as they claim, as taking the mean of the squared values
# does not create a norm. This method therfore does NOT create a unit vector
# I have sanity checked this a dozen times--this is the equation they publish
# in their paper, and it is the operation they execute in their code, too
# https://github.com/tkarras/progressive_growing_of_gans/blob/master/networks.py#L120
def pixelNorm(x: torch.Tensor) -> torch.Tensor:
    denominator: torch.Tensor = torch.sqrt(torch.mean(x**2, dim=1, keepdim=True) + __EPSILON)
    return x * (1 / denominator)


def computeWeightScale(layer: nn.Module) -> float:
    numWeights: int = 1
    for dim in layer.weight.shape[1:]:
        numWeights *= dim
    return math.sqrt(2 / numWeights)


# takes linear output of (N, C, W^2) and reshapes to (N, C, W, W)
def reshapeLinearBlockToConv(x: torch.Tensor) -> torch.Tensor:
    N, C, wSquared = x.shape
    W: int = int(math.sqrt(wSquared))
    return x.reshape((N, C, W, W))

def scaleTensorToRange(x: torch.Tensor, scaleRange: Tuple[float, float]) -> torch.Tensor:
    return (scaleRange[1] - scaleRange[0]) * ((x - torch.min(x)) / (torch.max(x) - torch.min(x) + __EPSILON)) + scaleRange[0]


def reshapeAndExpandEmbedding(labels: torch.Tensor, desiredH: int) -> torch.Tensor:
    labelHW: int = int(math.sqrt(labels.shape[-1]))
    labels = labels.reshape(labels.shape[0], 1, labelHW, labelHW)
    labels = F.interpolate(labels, scale_factor=desiredH // labelHW)
    return labels


if __name__ == "__main__":
    torch.manual_seed(5)
    testInput: torch.Tensor = torch.rand((1, 2, 2, 2)) * 10
    expectedOutput: torch.Tensor = torch.FloatTensor([[[0.9474, 1.0382],
                                                       [1.3916, 1.0512]],

                                                      [[1.0500, 0.9603],
                                                       [0.2521, 0.9460]]])
    testInput = pixelNorm(testInput)
    assert(torch.sum(testInput - expectedOutput) < __EPSILON)
    testReshapingInput: torch.Tensor = torch.rand((1, 8, 16))
    assert(reshapeLinearBlockToConv(testReshapingInput).shape == (1, 8, 4, 4))
    testLayer: nn.Conv2d = nn.Conv2d(10, 11, 3)
    assert(computeWeightScale(testLayer) - math.sqrt(2 / (10 * 3 * 3)) < __EPSILON)

    scaleRange: Tuple[float, float] = (8.448, 143)
    scaledValues: torch.Tensor = scaleTensorToRange(torch.rand((5, 5)) * 5, scaleRange)
    assert(torch.min(scaledValues) >= scaleRange[0] and torch.max(scaledValues) <= scaleRange[1])

    testInput: torch.Tensor = torch.zeros((2, 1, 4))
    testInput[:, :, 0] = 1
    testInput[:, :, 1] = 2
    testInput[:, :, 2] = 3
    testInput[:, :, 3] = 4

    expectedOutput: torch.Tensor = torch.FloatTensor([[[[1., 1., 2., 2.],
                                                        [1., 1., 2., 2.],
                                                        [3., 3., 4., 4.],
                                                        [3., 3., 4., 4.]]],


                                                      [[[1., 1., 2., 2.],
                                                        [1., 1., 2., 2.],
                                                        [3., 3., 4., 4.],
                                                        [3., 3., 4., 4.]]]])

    reshapedInput: torch.Tensor = reshapeAndExpandEmbedding(testInput, 4)
    assert(torch.sum(expectedOutput - reshapedInput) < __EPSILON)











