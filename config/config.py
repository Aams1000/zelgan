from typing import Tuple


class Config:

    # Network/module params
    class NN:
        BATCH_SIZE: int = 16
        EPOCHS_PER_STAGE: int = 60 * 3

        REGULARIZATION: float = 1e-2
        LEARNING_RATE: float = 5e-4
        LEARNING_RATE_FID: float = 1e-4

        LOG_FREQUENCY: int = 100
        SAVE_FREQUENCY: int = EPOCHS_PER_STAGE // 4 + 1
        SAMPLE_FREQUENCY: int = 10

        VALIDATE_FREQUENCY: int = 10

        NUM_ITERATIONS_FOR_AVERAGE: int = 10
        LATENT_FEATURES: int = 16

        LATENT_CHANNELS: int = 512 - LATENT_FEATURES

        INITIAL_RESOLUTION: int = 4
        BATCHES_PER_STAGE: int = 10
        MAX_STAGE: int = 6

        RANDOM_SEED: int = 7

        TEST_OUTPUT_BATCH_SIZE: int = 16

        ALPHA_INCREMENT: float = 0.0000037037 / 3

        SAMPLE_SIZE: int = 20 * 16

        GRADIENT_PENALTY_LAMBDA: int = 10
        EPSILON_PENALTY_LAMBDA: float = 0.001

        BETAS: Tuple[float, float] = (0.5, 0.9)

        FID_DIMENSIONS: Tuple[int, int] = (299, 299)
        INCEPTION_OUTPUT_SIZE: int = 2048

    #File/data config
    class IO:
        NUM_DATALOADER_THREADS: int = 5

