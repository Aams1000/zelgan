import torch
import torch.nn as nn
from models.generator import Generator
from models.nonauxillarydiscriminator import NonAuxillaryDiscriminator
from models.discriminator import Discriminator
from typing import Tuple
from fileio.customlogging import Logging
import fileio.loaders.model_io as model_io


def loadGenAndDisc(stage: int,
                   numLatentChannels: int,
                   numLatentFeatures: int,
                   initialResolution: int,
                   numClasses: int,
                   alphaIncrement: float,
                   device: torch.device,
                   shouldUseAuxillaryDisc: bool = True,
                   savedModelPath: str = None,
                   overrideAlpha: bool = False) -> Tuple[nn.Module, nn.Module]:
    initialStage: int = stage if not overrideAlpha else stage + 1
    generator: Generator = Generator(numLatentChannels,
                                     1,
                                     initialResolution,
                                     numClasses,
                                     initialStage=initialStage,
                                     alphaIncrement=alphaIncrement)

    discriminator: nn.Module = None
    if shouldUseAuxillaryDisc:
        Logging.log("Loading auxillary discriminator")
        discriminator: Discriminator = Discriminator(numClasses, initialStage=initialStage, alphaIncrement=alphaIncrement)
    else:
        Logging.log("Loading non-auxillary discriminator")
        discriminator: NonAuxillaryDiscriminator = NonAuxillaryDiscriminator(numClasses,
                                                                             numLatentFeatures,
                                                                             initialStage=initialStage,
                                                                             alphaIncrement=alphaIncrement)
    Logging.log("Configuring model for multi-GPU support")
    generator = nn.DataParallel(generator).to(device)
    discriminator = nn.DataParallel(discriminator).to(device)

    if savedModelPath is not None:
        Logging.log(f"Loading model save dicts from {savedModelPath}")
        generator.load_state_dict(model_io.loadGeneratorSaveDict(savedModelPath, device))
        discriminator.load_state_dict(model_io.loadDiscriminatorSaveDict(savedModelPath, device))

    return generator, discriminator


def loadGenAndDiscOptimizers(generator: nn.Module, discriminator: nn.Module, learningRate: float, betas: Tuple[float, float], device: torch.device, savedModelPath: str = None) -> Tuple[torch.optim.Optimizer, torch.optim.Optimizer]:
    genOptimizer: torch.optim.Optimizer = torch.optim.Adam(generator.parameters(), lr=learningRate, betas=betas)
    discOptimizer: torch.optim.Optimizer = torch.optim.Adam(discriminator.parameters(), lr=learningRate, betas=betas)
    if savedModelPath is not None:
        Logging.log(f"Loading optimizer save dicts from {savedModelPath}")
        genOptimizer.load_state_dict(model_io.loadGeneratorOptimizerStateDict(savedModelPath, device))
        discOptimizer.load_state_dict(model_io.loadDiscriminatorOptimizerStateDict(savedModelPath, device))
    else:
        Logging.log("Initializing new optimizers")

    return genOptimizer, discOptimizer



