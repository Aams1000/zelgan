import torch
import torch.nn as nn
from models.convleakypixel import ConvLeakyPixel
from models.rescaling_linear import RescalingLinear
import models.modelstatistics as modelstatistics
import models.tensoroperations as tensor_operations
from typing import Tuple
import math
from fileio.customlogging import Logging


class FinalDiscriminatorBlock(nn.Module):

    def __init__(self,
                 inChannels: int,
                 numFeatures: int = 4,
                 numClasses: int = -1):
        super(FinalDiscriminatorBlock, self).__init__()
        flattenedLength: int = inChannels * numFeatures**2
        self.clp: ConvLeakyPixel = ConvLeakyPixel(inChannels + 1, inChannels, usePixelNorm=False)
        self.linearOne: RescalingLinear = RescalingLinear(flattenedLength, 1)
        self.linearTwo: RescalingLinear = RescalingLinear(1, 1, useLeakyRelu=False)

        self.classPredictorOne: RescalingLinear = None
        self.classPredictorTwo: RescalingLinear = None
        if numClasses > 0:
            self.classPredictorOne = RescalingLinear(flattenedLength, numClasses * 2, useLeakyRelu=True)
            self.classPredictorTwo = RescalingLinear(numClasses * 2, numClasses, useLeakyRelu=False)

    def forward(self, x: torch.Tensor) -> Tuple[torch.Tensor, torch.Tensor]:
        # print(f"X: {x.shape}")
        if math.isnan(x[0, 0, 0, 0].item()):
            Logging.log("NAN")
        x = modelstatistics.appendMinibatchStdLayer(x)
        # print(f"Std appended: {x.shape}")
        x = self.clp(x)
        # print(f"Post conv: {x.shape}")
        x = tensor_operations.flatten(x)
        # print(f"Flattened: {x.shape}")
        realPredictions: torch.Tensor = self.linearTwo(self.linearOne(x))
        # print(f"Linear one: {x.shape}")
        classPredictions: torch.Tensor = self.classPredictorTwo(self.classPredictorOne(x)) if self.classPredictorOne is not None else None
        # print(f"Linear two: {x.shape}")
        # return torch.sigmoid(x)
        return realPredictions, classPredictions



if __name__ == "__main__":
    torch.manual_seed(5)
    testInput: torch.Tensor = torch.rand((8, 16, 4, 4))
    assert(FinalDiscriminatorBlock(16)(testInput)[0].shape == (8, 1))

