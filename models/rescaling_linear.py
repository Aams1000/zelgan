import torch
import torch.nn as nn
import torch.nn.functional as F
import models.weightscaling as weightscaling
import math
from fileio.customlogging import Logging


class RescalingLinear(nn.Module):

    def __init__(self,
                 inFeatures: int,
                 outFeatures: int,
                 useLeakyRelu: bool = True,
                 usePixelNorm: bool = False,
                 shouldReshapeAfterLinear: bool = False,
                 leakyReLUAlpha: float = 0.2):
        super(RescalingLinear, self).__init__()
        self.linear: nn.Linear = nn.Linear(inFeatures, outFeatures)
        nn.init.normal_(self.linear.weight)
        torch.nn.init.zeros_(self.linear.bias)
        self.useLeakyRelu: bool = useLeakyRelu
        self.usePixelNorm: bool = usePixelNorm
        self.shouldReshapeAfterLinear: bool = shouldReshapeAfterLinear
        self.weightScale: float = weightscaling.computeWeightScale(self.linear)
        self.leakyReLUAlpha: float = leakyReLUAlpha

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        #TODO - use no_grad? is this right?
        if math.isnan(x[0, 0].item()):
            Logging.log("NAN")
        x = self.linear(x) * self.weightScale
        if self.shouldReshapeAfterLinear:
            x = weightscaling.reshapeLinearBlockToConv(x)
        if self.useLeakyRelu:
            x = F.leaky_relu_(x, self.leakyReLUAlpha)
        if self.usePixelNorm:
            x = weightscaling.pixelNorm(x)
        return x

if __name__ == "__main__":
    testLayer: RescalingLinear = RescalingLinear(1, 16, shouldReshapeAfterLinear=True)
    testInput: torch.Tensor = torch.rand((2, 32, 1))
    testOutput: torch.Tensor = testLayer(testInput)
    assert(testOutput.shape == (2, 32, 4, 4))