import datetime
import time


class Logging:

    LOG_FILE = None

    @staticmethod
    def log(*args):
        message = ""
        for arg in args:
            message += str(arg) + " "
        timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('(%H:%M:%S) ')
        print(timestamp + message)
        if Logging.LOG_FILE is not None:
            with open(Logging.LOG_FILE, "a") as logFile:
                logFile.write(timestamp + message + "\n")
        else:
            raise Exception("Logger called without initializing log file. Please set Logging.LOG_FILE before use.")


    @staticmethod
    def generateLogFilePath(runId: str):
        return "./logs/" + runId + ".log"
