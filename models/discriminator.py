import torch
import torch.nn as nn
from models.final_discriminator_block import FinalDiscriminatorBlock
from models.fromrgbcconv import FromRGBCConv
from models.discriminatorblock import DiscriminatorBlock
import models.discriminatoroperations as disc_ops
import models.generator_operations as gen_ops
from typing import Tuple, Dict
import math
from fileio.customlogging import Logging


class Discriminator(nn.Module):

    def __init__(self,
                 numClassIds: int,
                 maxChannels: int = 512,
                 numMainBlocks: int = 6,
                 alphaIncrement: float = 0.000125,
                 scaleFactor: int = 2,
                 initialStage: int = 0,
                 numInputChannels: int = 3):
        super(Discriminator, self).__init__()
        self.discriminatorBlocks: nn.ModuleList = nn.ModuleList([FinalDiscriminatorBlock(maxChannels, numClasses=numClassIds)])
        self.rgbcBlocks: nn.ModuleList = nn.ModuleList([FromRGBCConv(maxChannels, inChannels=numInputChannels)])

        blocksPlusRgbcs: Tuple[nn.ModuleList, nn.ModuleList] = self.constructMainBlocksAndRGBs(maxChannels, numMainBlocks, numInputChannels)
        self.discriminatorBlocks.extend(blocksPlusRgbcs[0])
        self.rgbcBlocks.extend(blocksPlusRgbcs[1])
        self.scaleFactor: int = scaleFactor
        self.alphaIncrement: float = alphaIncrement

        # fade-in applies only to blockIds >= 1. Using a dictionary to avoid confusing indexing
        self.alphas: Dict[int, float] = gen_ops.initializeAlphas(initialStage, alphaIncrement, numMainBlocks)
        print(self.alphas)

    @staticmethod
    def constructMainBlocksAndRGBs(maxChannels: int, numMainBlocks: int, numInputChannels: int) -> Tuple[nn.ModuleList, nn.ModuleList]:
        discriminatorBlocks: nn.ModuleList = nn.ModuleList()
        fromRgbcBlocks: nn.ModuleList = nn.ModuleList()
        for i in range(0, numMainBlocks):
            inputChannels: int = maxChannels // 2**(i)
            outputChannels: int = min(inputChannels * 2, maxChannels)
            fromRgbcBlocks.append(FromRGBCConv(inputChannels, inChannels=numInputChannels))
            discriminatorBlocks.append(DiscriminatorBlock(inputChannels, outputChannels))

        return discriminatorBlocks, fromRgbcBlocks

    def forward(self, x: torch.Tensor, stage: int) -> Tuple[torch.Tensor, torch.Tensor]:
        if math.isnan(x[0, 0, 0, 0].item()):
            Logging.log("NAN")
        if stage > 0:
            alpha: float = self.alphas[stage]
            # print(alpha)
            if alpha < 1.0:
                # print("Scaling...d")
                x = disc_ops.computeWeightedOutput(x, stage, alpha, self.scaleFactor, self.discriminatorBlocks, self.rgbcBlocks)
                gen_ops.updateAlpha(self.alphas, stage, x.shape[0], self.alphaIncrement)
                if math.isnan(x[0, 0, 0, 0].item()):
                    Logging.log("NAN")
            else:
                # print(f"X: {x.shape}")
                x = disc_ops.rgbcPlusBlock(x, stage, self.discriminatorBlocks, self.rgbcBlocks)
                if math.isnan(x[0, 0, 0, 0].item()):
                    Logging.log("NAN")
            for i in reversed(range(1, stage)):
                # print(f"Executing block {i}...")
                x = self.discriminatorBlocks[i](x)
                if math.isnan(x[0, 0, 0, 0].item()):
                    Logging.log("NAN")
        else:
            x = self.rgbcBlocks[stage](x)
            if math.isnan(x[0, 0, 0, 0].item()):
                Logging.log("NAN")
        if math.isnan(x[0, 0, 0, 0].item()):
            Logging.log("NAN")
        return self.discriminatorBlocks[0](x)


if __name__ == "__main__":
    testInput: torch.Tensor = torch.rand((4, 3, 8, 8))
    (Discriminator(10))(testInput, 1)



