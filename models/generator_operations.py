import torch
import torch.nn as nn
import torch.nn.functional as F
from models.generatorblock import GeneratorBlock
from models.torgbconv import ToRGBConv
from typing import Tuple, Dict


def computeWeightedOutput(x: torch.Tensor,
                          stage: int,
                          alpha: float,
                          scaleFactor: float,
                          blocks:nn.ModuleList,
                          rgbs: nn.ModuleList) -> torch.Tensor:
    # print(x[0, 0])
    # previousOutput: torch.Tensor = F.interpolate(rgbs[stage - 1](x), scale_factor=scaleFactor, mode="nearest")
    # print(x[0, 0])
    # newOutput: torch.Tensor = rgbs[stage](blocks[stage](x))

    return torch.add((1 - alpha) * (F.interpolate(rgbs[stage - 1](x), scale_factor=scaleFactor, mode="nearest")), alpha * rgbs[stage](blocks[stage](x)))


def updateAlpha(alphas: Dict[int, float], stage: int, numImages: int, unitIncrement: float) -> None:
    alphas[stage] += numImages * unitIncrement


def constructMainBlocksAndRGBs(latentChannels: int,
                                 cFactor: int,
                                 numMainBlocks: int) -> Tuple[nn.ModuleList, nn.ModuleList]:
    generatorBlocks: nn.ModuleList = nn.ModuleList()
    rgbBlocks: nn.ModuleList = nn.ModuleList()
    generatorBlocks.append(GeneratorBlock(latentChannels, latentChannels))
    rgbBlocks.append(ToRGBConv(latentChannels))
    for i in range(0, numMainBlocks - 1):
        inChannels: int = latentChannels // cFactor**i
        outChannels: int = latentChannels // cFactor**(i + 1)
        generatorBlocks.append(GeneratorBlock(inChannels, outChannels))
        rgbBlocks.append(ToRGBConv(outChannels))
    return generatorBlocks, rgbBlocks


def blockPlusRGB(x: torch.Tensor, stage: int, blocks: nn.ModuleList, rgbs: nn.ModuleList) -> torch.Tensor:
    x = blocks[stage](x)
    x = rgbs[stage](x)
    return x


def initializeAlphas(initialStage: int, alphaIncrement: float, numMainBlocks: int) -> Dict[int, float]:
    # fade-in applies only to blockIds >= 1. Using a dictionary to avoid confusing indexing
    alphas: Dict[int, float] = {blockId: alphaIncrement for blockId in range(1, numMainBlocks + 1)}
    if initialStage > 0:
        for i in range(1, initialStage):
            alphas[i] = 1.0
    # print(alphas)
    return alphas

