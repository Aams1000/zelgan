import torch
import torch.nn as nn
import torch.nn.functional as F
from models.convleakypixel import ConvLeakyPixel
from typing import Tuple
import math
from fileio.customlogging import Logging


class DiscriminatorBlock(nn.Module):

    def __init__(self,
                 inChannels: int,
                 outChannels: int,
                 kernelSize: int = 3,
                 padding: int = 1,
                 stride: int = 1):
        super(DiscriminatorBlock, self).__init__()
        self.clpOne: ConvLeakyPixel = ConvLeakyPixel(inChannels, outChannels, kernelSize, padding, stride, usePixelNorm=False)
        self.clpTwo: ConvLeakyPixel = ConvLeakyPixel(outChannels, outChannels, kernelSize, padding, stride, usePixelNorm=False)
        self.downscaleFactor: Tuple[int, int] = (2, 2)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        if math.isnan(x[0, 0, 0, 0].item()):
            Logging.log("NAN")
        # print(f"Input: {x.shape}")
        # x = F.interpolate(x, scale_factor=self.upscaleFactor, mode="nearest")
        x = self.clpOne(x)
        x = self.clpTwo(x)
        x = F.avg_pool2d(x, self.downscaleFactor, stride=self.downscaleFactor)
        # print(f"Output: {x.shape}")
        return x


if __name__ == "__main__":
    torch.manual_seed(5)
    testInput: torch.Tensor = torch.rand((2, 2, 10, 10))
    testBlock: DiscriminatorBlock = DiscriminatorBlock(2, 4)
    assert(testBlock(testInput).shape == (2, 4, 5, 5))


